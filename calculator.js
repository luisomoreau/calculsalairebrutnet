angular.module('CalculatorApp', [])
    .controller('CalculatorController', function($scope) {

        //sources :
        //http://travail-emploi.gouv.fr/droit-du-travail/remuneration-et-participation-financiere/remuneration/article/la-mensualisation
        //http://www.journaldunet.com/management/salaire-cadres/1071810-smic/
        //http://droit-finances.commentcamarche.net/faq/3567-smic-2016-taux-horaire-et-smic-mensuel

        $scope.init = function(){
            $scope.smic=[
                {date: "2016", valeur:9.67},
                {date: "2015", valeur:9.61},
                {date: "2014", valeur:9.53},
                {date: "2013", valeur:9.43},
                {date: "2012", valeur:9.40},
                {date: "2011", valeur:9.22},
                {date: "2010", valeur:8.86},
                {date: "2009", valeur:8.82},
                {date: "2008", valeur:8.63},
                {date: "2007", valeur:8.44},
                {date: "2006", valeur:8.27},
                {date: "2005", valeur:8.03}
            ];

            //$scope.horaireBrut = 0;
            //$scope.horaireNet = 0;
            //$scope.mensuelBrut = 0;
            //$scope.mensuelNet = 0;
            //$scope.annuelBrut = 0;
            //$scope.annuelNet = 0;

            $scope.heures = 35;
            $scope.semaines = 52;
            $scope.mois = 12;

            $scope.chargesSalarial = 23;
            $scope.privee = true;

        }


        $scope.init();


        $scope.resultHeures = function(){
            $scope.annuelBrut = $scope.horaireBrut * $scope.heures * $scope.semaines;
            $scope.mensuelBrut = ($scope.horaireBrut * $scope.heures * $scope.semaines) / $scope.mois;

            $scope.annuelNet = $scope.horaireNet * $scope.heures * $scope.semaines;
            $scope.mensuelNet = $scope.horaireNet * $scope.heures * $scope.semaines / $scope.mois;
        };

        $scope.resultMois = function(){
            $scope.annuelBrut = $scope.mensuelBrut * $scope.mois;
            $scope.annuelNet = $scope.mensuelNet * $scope.mois;
        }


        $scope.result = function(){
        };

        $scope.resultFromHB = function() {
            $scope.annuelBrut = $scope.horaireBrut * $scope.heures * $scope.semaines;
            $scope.mensuelBrut = ($scope.horaireBrut * $scope.heures * $scope.semaines) / $scope.mois;

            $scope.horaireNet = $scope.horaireBrut * (1 - $scope.chargesSalarial/100);
            $scope.annuelNet = $scope.horaireNet * $scope.heures * $scope.semaines;
            $scope.mensuelNet = $scope.horaireNet * $scope.heures * $scope.semaines / $scope.mois;
        };

        $scope.resultFromMB = function() {
            $scope.annuelBrut = ($scope.mensuelBrut * $scope.mois);
            $scope.horaireBrut = ($scope.mensuelBrut * $scope.mois) / $scope.heures / $scope.semaines;

            $scope.horaireNet = $scope.horaireBrut * (1 - $scope.chargesSalarial/100);
            $scope.annuelNet = $scope.horaireNet * $scope.heures * $scope.semaines;
            $scope.mensuelNet = $scope.horaireNet * $scope.heures * $scope.semaines / $scope.mois;


        };

        $scope.resultFromAB = function() {
            $scope.horaireBrut = $scope.annuelBrut / ($scope.heures * $scope.semaines);
            $scope.mensuelBrut = $scope.annuelBrut / $scope.mois;

            $scope.horaireNet = $scope.horaireBrut * (1 - $scope.chargesSalarial/100);
            $scope.annuelNet = $scope.horaireNet * $scope.heures * $scope.semaines;
            $scope.mensuelNet = $scope.horaireNet * $scope.heures * $scope.semaines / $scope.mois;

        };

        $scope.resultFromHN = function(){
            $scope.annuelNet = $scope.horaireNet * $scope.heures * $scope.semaines;
            $scope.mensuelNet = $scope.horaireNet * $scope.heures * $scope.semaines / $scope.mois;

            $scope.horaireBrut = $scope.horaireNet / (1 - $scope.chargesSalarial/100)
            $scope.annuelBrut = $scope.horaireBrut * $scope.heures * $scope.semaines;
            $scope.mensuelBrut = ($scope.horaireBrut * $scope.heures * $scope.semaines) / $scope.mois;
        }

        $scope.resultFromMN = function(){
            $scope.annuelNet = $scope.mensuelNet * $scope.mois;
            $scope.horaireNet = ($scope.mensuelNet * $scope.mois) / $scope.heures / $scope.semaines;

            $scope.horaireBrut = $scope.horaireNet / (1 - $scope.chargesSalarial/100)
            $scope.annuelBrut = $scope.horaireBrut * $scope.heures * $scope.semaines;
            $scope.mensuelBrut = ($scope.horaireBrut * $scope.heures * $scope.semaines) / $scope.mois;
        }

        $scope.resultFromAN = function(){
            $scope.horaireNet = $scope.annuelNet / ($scope.heures * $scope.semaines);
            $scope.mensuelNet = $scope.annuelNet / $scope.mois;

            $scope.horaireBrut = $scope.horaireNet / (1 - $scope.chargesSalarial/100)
            $scope.annuelBrut = $scope.horaireBrut * $scope.heures * $scope.semaines;
            $scope.mensuelBrut = ($scope.horaireBrut * $scope.heures * $scope.semaines) / $scope.mois;
        }

        $scope.secteur = function(){
            if($scope.privee==true){
                $scope.chargesSalarial = 23;
                $scope.horaireNet = $scope.horaireBrut * (1 - $scope.chargesSalarial/100);
                $scope.annuelNet = $scope.horaireNet * $scope.heures * $scope.semaines;
                $scope.mensuelNet = $scope.horaireNet * $scope.heures * $scope.semaines / $scope.mois;
            }
            else{
                $scope.chargesSalarial = 15;
                $scope.horaireNet = $scope.horaireBrut * (1 - $scope.chargesSalarial/100);
                $scope.annuelNet = $scope.horaireNet * $scope.heures * $scope.semaines;
                $scope.mensuelNet = $scope.horaireNet * $scope.heures * $scope.semaines / $scope.mois;
            }
        }


    });